import {debounce} from 'lodash';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {updateVideos} from '../actions/actions';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            term: ''
        };

        this.onSearchChange = this.onSearchChange.bind(this);
        this.searchVideos = debounce(this.searchVideos, 500);
    }

    render() {
        return (
            <div className="search-bar">
                <input
                    value={this.state.term}
                    onChange={this.onSearchChange}
                />
            </div>
        );
    }

    onSearchChange(event) {
        const term = event.target.value;

        this.setState({term});
        this.searchVideos(term);
    }

    searchVideos(term) {
        this.props.updateVideos(term);
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({updateVideos}, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);