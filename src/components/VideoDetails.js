import React, {Component} from 'react';
import {connect} from 'react-redux';

class VideoDetails extends Component {
    render() {
        if (this.props.video) {
            return this.renderVideoDetails();
        } else {
            return this.renderLoader();
        }
    }

    renderVideoDetails() {
        const {video} = this.props;

        const videoId = video.id.videoId;
        const url = `https://www.youtube.com/embed/${videoId}`;

        return (
            <div className="col-md-8">
                <div className="embed-responsive embed-responsive-16by9">
                    <iframe className="embed-responsive-item" src={url} title="youtube video" />
                </div>
                <div className="details">
                    <h5>{video.snippet.title}</h5>
                    <div>{video.snippet.description}</div>
                </div>
            </div>
        );
    }

    renderLoader() {
        return <div>Loading...</div>;
    }
}

function mapStateToProps(state) {
    return {
        video: state.video
    };
}

export default connect(mapStateToProps)(VideoDetails);