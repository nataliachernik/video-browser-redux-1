import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import SearchBar from './components/SearchBar';
import VideoDetails from './components/VideoDetails';
import VideoList from './components/VideoList';

import { updateVideos } from './actions/actions';

import './App.css';

const DEFAULT_TERM = 'surfboards';

class App extends Component {
    componentDidMount() {
        this.props.updateVideos(DEFAULT_TERM);
    }

    render() {
        return (
            <div className="container">
                <SearchBar />
                <div className="row">
                    <VideoDetails />
                    <VideoList
                        onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                        videos={this.props.videos}
                    />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        videos: state.videos
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({updateVideos}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);