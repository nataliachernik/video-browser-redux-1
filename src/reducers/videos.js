export const UPDATE_VIDEOS = 'UPDATE_VIDEOS';

export const Videos = (state = [], action) => {
    switch (action.type) {
        case UPDATE_VIDEOS:
            return action.videos;
        default:
            return state;
    }
};

export default Videos;