import {combineReducers} from 'redux';

import Videos from './videos';
import Video from './video';

const rootReducer = combineReducers({
    videos: Videos,
    video: Video
});

export default rootReducer;
