import React from 'react';
import {render} from 'react-dom';
import Root from './root/Root';
import configureStore from './store/configureStore';

import 'bootstrap/dist/css/bootstrap.min.css';

const store = configureStore();

render(
    <Root store={store} />,
    document.getElementById('root')
);